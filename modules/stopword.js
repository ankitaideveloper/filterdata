var word = ['ourselves', 'hers', 'between', 'yourself', 'but', 'again', 'there', 'about', 'once', 'during', 'out', 'very', 'having', 'with', 'they', 'own', 'an', 'be', 'some', 'for', 'do', 'its', 'yours', 'such', 'into', 'of', 'most', 'itself', 'other', 'off', 'is', 's', 'am', 'or', 'who', 'as', 'from', 'him', 'each', 'the', 'themselves', 'until', 'below', 'are', 'we', 'these', 'your', 'his', 'through', 'don', 'nor', 'me', 'were', 'her', 'more', 'himself', 'this', 'down', 'should', 'our', 'their', 'while', 'above', 'both', 'up', 'to', 'ours', 'had', 'she', 'all', 'no', 'when', 'at', 'any', 'before', 'them', 'same', 'and', 'been', 'have', 'in', 'will', 'on', 'does', 'yourselves', 'then', 'that', 'because', 'what', 'over', 'why', 'so', 'can', 'did', 'not', 'now', 'under', 'he', 'you', 'herself', 'has', 'just', 'where', 'too', 'only', 'myself', 'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being', 'if', 'theirs', 'my', 'against', 'a', 'by', 'doing', 'it', 'how', 'further', 'was', 'here', 'than']
var Stopword = {
    getStopword: function (userQuery) {
        var UserArray1=userQuery.toLowerCase();
        var userArraycheck=Stopword.checkIsArray(UserArray1);
        var removeDuplicateStopwords=Stopword.removeDuplicate(userArraycheck);
        return removeDuplicateStopwords;
    },
    checkIsArray: function (userArray) {
        var userArrayResponse = userArray;
        if (Array.isArray(userArray) != true) {
            var temp = userArray.split(' ');
            var makeArray = [];
            for (var i = 0; i < temp.length; i++) {
                makeArray.push(temp[i])
            }
            userArrayResponse = makeArray;
        }
        return userArrayResponse;
    },
    removeDuplicate: function(userQuery)
    {
for (var i = 0; i<word.length; i++) {
    var arrlen = userQuery.length;
    for (var j = 0; j<arrlen; j++) {
        if (word[i] == userQuery[j]) {
            userQuery = userQuery.slice(0, j).concat(userQuery.slice(j+1, arrlen));
        }
    }
}
return userQuery;
    }
}
module.exports = Stopword;