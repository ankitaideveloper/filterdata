var stopword= require('./modules/stopword.js');
var specialChar= require('./modules/specialSymbol.js')
exports.removeStopwords= function(query)
{
    var removeStopwordsResponse= stopword.getStopword(query);
    return removeStopwordsResponse;
}
exports.removeSpecialChar= function(query)
{
    var removeSpecialChar= specialChar.getSpecialSymbol(query);
    return removeSpecialChar;
}
console.log(specialChar.getSpecialSymbol("theo # % 1@ ^"));